import socket
import os
from os import walk
from os import listdir
from os.path import isfile, join
import time
import threading
from graphics import Graphics, Popup
import zlib
import filecmp

received = []
global data
data = ""
global isOk
isOk = True

def recv(s):
    while 1:
        global isOk
        if isOk == True:
            data = s.recv(1024).decode("UTF-8")
            split = data.split()
            if split[0] == "5":
                filename = split[1]
                len = int(split[2])
                cnt = 0
                msg11 = "".encode("UTF-8")
                with open(getUsername() + "\\" + filename, 'wb') as filesent:
                    while cnt < len:
                        data = s.recv(len)
                        msg11 = msg11 + data
                        cnt = cnt + data.__len__()
                    msg11 = decomp(msg11)
                    filesent.write(msg11)
                global received
                received.append(filename)
                Popup("You've got a new file: " + filename)

            else:
                isOk = False




def comp(data):
    compress = zlib.compressobj(zlib.Z_DEFAULT_COMPRESSION, zlib.DEFLATED, +15)
    compressed_data = compress.compress(data)
    compressed_data += compress.flush()
    return compressed_data

def decomp(data):
    data2 = zlib.decompressobj()
    decompressed_data = data2.decompress(data)
    decompressed_data += data2.flush()
    return decompressed_data


def log_in(username, password):
    with open("islog.csv", 'w+') as f:
        f.write("user: " + username + " password: " + password)


def checkIfLogged(s):
    try:
        with open("islog.csv", "r") as f:
            data = f.read(1024)
            spl = data.split()
            user = spl[1]
            password = spl[3]
            s.send(("1 " + user + " " + password).encode("UTF-8"))
            d = s.recv(1024).decode("UTF-8")
            if d == "invalid":
                try:
                    os.remove("islog.csv")
                    return False
                except:
                    return False
            else:
                return True
    except Exception as e:
        return False


def getUsername():
    with open("islog.csv", "r") as f:
        data = f.read(1024)
        spl = data.split()
        user = spl[1]
        return user


def checkIfChanged(files,folds):
    ret1 = []
    ret2 = []

    onlyfiles = [f for f in listdir(getUsername() + "\\") if isfile(join(getUsername() + "\\", f))]
    for i in onlyfiles:
        if i not in files and i not in received:
            ret1.append(i)
            break

    curfolds = [ f.path for f in os.scandir(getUsername() + "\\") if f.is_dir() ]
    for i in curfolds:
        if i not in folds and i not in received:
            split = i.split("\\")
            ret2.append(split[1])
            break

    num1 = 0
    num2 = 0

    for i in ret1:
        num1 += 1

    for i in ret2:
        num2 += 1

    if num1 == 0 and num2 == 0:
        time.sleep(3)
        return checkIfChanged(onlyfiles,curfolds)
    else:
        return (ret1,ret2)


def sending():
    while 1:
        dir = [f for f in listdir(getUsername() + "\\") if isfile(join(getUsername() + "\\", f))]
        folds = [ f.path for f in os.scandir(getUsername() + "\\") if f.is_dir() ]
        for i in folds:
            print (i)
        (files,dirs) = checkIfChanged(dir,folds)
        filenames = ""
        for filename in files:
            print (filename)
            while (1):
                with open(getUsername() + "\\" + filename, 'rb') as filesent:
                    data = filesent.read()
                    rdata = comp(data)
                    file_len = len(rdata)
                    print(file_len)
                    s.send(("3 " + getUsername() + " " + filename + " " + str(file_len)).encode("UTF-8"))
                    time.sleep(0.3)
                    s.sendall(rdata)
                break
            filenames += filename + " "

        if filenames != "":
            Popup("You've sent: " + filenames)

        dirsnames = ""
        for folder in dirs:
            print (folder)
            s.send(("6 " + getUsername() + " " + folder).encode("UTF-8"))

            dir = [f for f in listdir(getUsername() + "\\" + folder + "\\") if isfile(join(getUsername() + "\\" + folder + "\\", f))]
            for filename in dir:
                print (filename)
                while (1):
                    with open(getUsername() + "\\" + folder + "\\" + filename, 'rb') as filesent:
                        data = filesent.read()
                        rdata = comp(data)
                        file_len = len(rdata)
                        print(file_len)
                        s.send(("3 " + getUsername() + " " + folder + "\\" + filename + " " + str(file_len)).encode("UTF-8"))
                        s.sendall(rdata)
                    break
            dirsnames += folder + " "

        global isOk
        isOk = True

        if dirsnames != "":
            Popup("You've sent: " + dirsnames)


def receiving():
    onlyfiles = [f for f in listdir(getUsername() + '\\') if isfile(join(getUsername() + '\\', f))]
    mess = ""
    for i in onlyfiles:
        mess = mess + i + " "
    if mess == "":
        s.send((str(program) + " " + getUsername() + " " +  "nothing").encode("UTF-8"))
    else:
        s.send((str(program) + " " + getUsername() + " " + mess).encode("UTF-8"))
    data = s.recv(1024).decode("UTF-8")
    files = []
    if data == "start":
        while 1:
            data = s.recv(1024).decode("UTF-8")
            if data == "done":
                break
            split = data.split()
            filename = split[0]
            len = int(split[1])
            files.append(filename)
            msg11 = "".encode("UTF-8")
            with open(getUsername() + "\\" + filename, 'wb') as filesent:
                cnt = 0
                while cnt < len:
                    data = s.recv(len)
                    msg11 = msg11 + data
                    cnt = cnt + data.__len__()
                filesent.write(msg11)

    msg = ""
    for f in files:
        msg = msg + f + "\n"
    if msg != "":
        Popup("You've got these files:\n" + msg)



s = socket.socket()             # Create a socket object
host = "localhost"  #Ip address that the TCPServer  is there
port = 50010                    # Reserve a port for your service every new transfer wants a new port or you must wait.

try:
    s.connect((host, port))
except Exception as e:
    print (e)

program = 0

if checkIfLogged(s) == True:
    program = 4
else:
    g = Graphics(s)
    action = g.getAction()
    if action == "Login":
        program = 4
    elif action == "Signup":
        program = 3

# s.send(str(program).encode("UTF-8"))

while 1:

    # if program == 2:
    #     username = input("Please enter your username: ")
    #     password = input("Please enter your password: ")
    #     s.send((str(program) + " " + username + " " + password).encode("UTF-8"))
    #     data = s.recv(1024).decode("UTF-8")
    #     print (data)
    #     while data != "valid":
    #         print ("username already taken")
    #         username = input("Please enter your username: ")
    #         password = input("Please enter your password: ")
    #         s.send((str(program) + " " + username + " " + password).encode("UTF-8"))
    #         data = s.recv(1024).decode("UTF-8")
    #
    #     data = s.recv(1024).decode("UTF-8")
    #     if data == "good":
    #         print("You're signed up!")
    #         log_in(username,password)
    #         path = username + "\\"
    #         os.mkdir(path)
    #         program = 3
    #     else:
    #         print ("there was a problem, please try again later")
    #
    # if program == 1:
    #     while 1:
    #         username = input("Please enter your username: ")
    #         password = input("Please enter your password: ")
    #         s.send((username + " , " + password).encode("UTF-8"))
    #         data = s.recv(1024).decode("UTF-8")
    #         if data == "fine":
    #             data = s.recv(1024).decode("UTF-8")
    #             if data == "valid":
    #                 log_in(username,password)
    #                 print ("logged in!")
    #                 program = 3
    #             else:
    #                 print ("either username or password were invalid, try again")

    if program == 4:
        receiving()
        program = 3

    if program == 3:

        t = threading.Thread(target=recv,args=[s])
        t.start()

        sending()
        # t1 = threading.Thread(target=sending,args=[program])
        # t2 = threading.Thread(target=receiving)

        # t1.start()
        # t2.start()
    #     dir = [f for f in listdir(r"user4\\") if isfile(join(r"user4\\", f))]
    #     filename = checkIfChanged(dir)
    #     while (1):
    #             print (filename)
    #             s.send(getUsername().encode("UTF-8"))
    #             s.send(filename.encode("UTF-8"))
    #             f = open(getUsername() + "\\" + filename, 'rb')
    #             l = f.read(1024)
    #             while (l):
    #                 print (l)
    #                 s.send(l)
    #                 print('Sent ', repr(l))
    #                 l = f.read(1024)
    #             f.close()
    #             s.send("done".encode("UTF-8"))
    #             break
    #     f.close()
    #     print('Done sending')
    #
    # if program == 4:
    #     data = s.recv(1024).decode("UTF-8")
    #     files = []
    #     if data == "start":
    #         data = s.recv(1024).decode("UTF-8")
    #         while data != "done":
    #             files.append(data)
    #             data = s.recv(1024).decode("UTF-8")
    #     onlyfiles = [f for f in listdir(r"user4\\") if isfile(join(r"user4\\", f))]
    #     notInDir = []
    #     for i in files:
    #         if i not in onlyfiles:
    #             notInDir.append(i)
    #             s.send(i.encode("UTF-8"))
    #     s.send("done".encode("UTF-8"))
    #     s.send(getUsername().encode("UTF-8"))
    #     while 1:
    #         filename = s.recv(1024).decode("UTF-8")
    #         print(filename)
    #         isDone = False
    #         if filename == "done":
    #             break
    #         with open(getUsername() + "/" + filename, 'wb') as f:
    #             print('file opened')
    #             isFinished = False
    #             while True:
    #                 print('receiving data...')
    #                 data = s.recv(1024)
    #                 if data == b'done1':
    #                     break
    #                 if data == b'done':
    #                     isFinished = True
    #                 print('data=%s', (data))
    #                 f.write(data)
    #             if isFinished == True:
    #                 break
