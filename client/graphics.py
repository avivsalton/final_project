from tkinter import Tk, Label, Button, Text
from tkinter.messagebox import showinfo
import socket
import os
from PyQt5.QtWidgets import QApplication
import sys
import random

class Graphics():

    def __init__(self,socket):
        self.socket = socket
        master = Tk()
        self.master = master
        self.action = ""
        master.title("Log In / Sign Up")
        master.geometry("500x500")
        master.resizable(0, 0)

        self.label = Label(master, text="Select your option:")
        self.label.pack()

        self.greet_button = Button(master, text="Log in", command=self.login)
        self.greet_button.pack()

        self.close_button = Button(master, text="Sign Up", command=self.signup)
        self.close_button.pack()

        self.master.mainloop()

    def getAction(self):
        return self.action

    def log_in(self, username, password):
        with open("islog.csv", 'w+') as f:
            f.write("user: " + username + " password: " + password)

    def setMaster(self,master):
        self.master = master

    def checkLogin(self,usermail,password,curroot):
        usermail = usermail.split()[0]
        password = password.split()[0]
        self.socket.send((str(1) + " " + usermail + " " + password).encode("UTF-8"))
        data = self.socket.recv(1024).decode("UTF-8")
        if data.split()[0] == "valid":
            username = data.split()[1]
            self.log_in(username, password)
            showinfo("Log in Window", "You're logged in!")
            self.action = "Login"
            path = username + "\\"
            try:
                os.mkdir(path)
            except:
                print (":(")
            curroot.destroy()
        else:
            showinfo("Log in Window","either username or password were invalid, try again")

    def checkSignup(self,username,email,password,curroot):
        username = username.split()[0]
        email = email.split()[0]
        password = password.split()[0]
        self.socket.send((str(2) + " " + username + " " + email + " " + password).encode("UTF-8"))
        data = self.socket.recv(1024).decode("UTF-8")
        if data == "valid":
            showinfo("Sign up Window","You're signed up!")
            self.action = "Signup"
            data = self.socket.recv(1024).decode("UTF-8")

            if data == "good":
                self.log_in(username, password)
                path = username + "\\"
                os.mkdir(path)
                curroot.destroy()
            else:
                showinfo("Sign up Window", "there was a problem, please try again later")
        else:
            showinfo("Sign up Window","Username is not valid!")

    def greet(self):
        print("Greetings!")

    def login(self):
        self.master.destroy()

        newroot = Tk()
        newroot.title("Log In")
        newroot.geometry("500x500")

        Label(newroot,text="Username\Email: ").pack()
        usermail = Text(newroot, height = 1, width = 20)
        usermail.pack()

        Label(newroot, text="Password: ").pack()
        password = Text(newroot, height=1, width=20)
        password.pack()


        submit = Button(newroot, text="Submit", command= lambda: self.checkLogin(usermail.get("1.0","end"),password.get("1.0","end"),newroot))
        submit.pack()

        fpass = Button(newroot, text="Forgot Password?", command= lambda: self.forgotPassword(newroot))
        fpass.pack()

        newroot.mainloop()

    def forgotPassword(self,root):

        root.destroy()

        newroot = Tk()
        newroot.title("Forgot Password")
        newroot.geometry("500x500")

        Label(newroot, text="Email/Username: ").pack()
        usermail = Text(newroot, height=1, width=20)
        usermail.pack()

        submit = Button(newroot, text="Submit", command=lambda: self.checkForgotPassword(usermail.get("1.0","end"), newroot))
        submit.pack()

        newroot.mainloop()

    def checkForgotPassword(self,usermail,root):

        passcode = random.randint(10000,99999)
        self.socket.send(("5 " + usermail + " " + str(passcode)).encode("UTF-8"))

        data = self.socket.recv(1024).decode("UTF-8")
        if data == "valid":

            root.destroy()

            newroot = Tk()
            newroot.title("Forgot Password")
            newroot.geometry("500x500")

            Label(newroot, text="Passcode: ").pack()
            passc = Text(newroot, height=1, width=20)
            passc.pack()

            submit = Button(newroot, text="Submit", command=lambda: self.changePassword(passcode,int(passc.get("1.0", "end").split()[0]), newroot))
            submit.pack()

            newroot.mainloop()
        else:
            showinfo("Forgot Password Window", "Username/Email does not exist!")


    def changePassword(self,passcode,passc,root):

        if passc == passcode:
            root.destroy()

            newroot = Tk()
            newroot.title("Forgot Password")
            newroot.geometry("500x500")

            Label(newroot, text="Passcode: ").pack()
            password = Text(newroot, height=1, width=20)
            password.pack()

            submit = Button(newroot, text="Submit", command=lambda: self.loginPass(password.get("1.0","end"),newroot))
            submit.pack()

            newroot.mainloop()
        else:
            showinfo("Forgot Password Window", "Incorrect Passcode!")

    def loginPass(self,password,root):

        self.socket.send(password.encode("UTF-8"))
        data = self.socket.recv(1024).decode("UTF-8").split()

        if data[0] == "valid":
            self.log_in(data[1], password)
            root.destroy()
            self.action = "Login"


    def signup(self):
        self.master.destroy()

        newroot = Tk()
        newroot.title("Sign Up")
        newroot.geometry("500x500")

        Label(newroot, text="Username: ").pack()
        username = Text(newroot, height=1, width=20)
        username.pack()

        Label(newroot, text="Email: ").pack()
        email = Text(newroot, height=1, width=20)
        email.pack()

        Label(newroot, text="Password: ").pack()
        password = Text(newroot, height=1, width=20)
        password.pack()

        submit = Button(newroot, text="Submit", command=lambda: self.checkSignup(username.get("1.0", "end"), email.get("1.0", "end"), password.get("1.0", "end"),newroot))
        submit.pack()

        newroot.mainloop()

class Popup():

    def __init__(self,message):

        root = Tk()
        app = QApplication(sys.argv)
        dw = app.desktop()  # dw = QDesktopWidget() also works if app is created
        taskbar_height = dw.availableGeometry().height() - 200
        root.geometry('500x200+0+' + str(taskbar_height))
        Label(root, text=message, font=("Arial",13)).pack()
        Button(root, text="Okay", command=lambda: root.destroy()).pack()
        root.overrideredirect(True)
        root.mainloop()

